# README #

The script `app.R` creates an example shiny app using R package `shiny`.

To build the app:

> `source("app.R")`

> `runApp()`
